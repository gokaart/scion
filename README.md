# SCION
## Script that Compares INEGI and OSM Names
##### Requirements
JOSM v.15553 (min.)
OSM Scripting plugin: v. 30796(min.)

##### Installation
* Download SCION for your project and save it somewhere you won’t lose it
* Install the scripting plugin on JOSM
    -JOSM -> Main -> Preferences -> Plugins -> scripting
* Set hotkey(s)
    -JOSM -> Main -> Preferences -> Keyboard Shortcuts -> search: scripting
    -Set SHOW/HIDE SCRIPTING CONSOLE to F5 (optional: set Run a Script)
* Hit OK and restart JOSM
* Load your layers into JOSM via Tasking Manager 
* Hit F5: you should have this console pop up. You can move the line in the middle so that you can see more of the output window. This is where your missing names will show up.
* In your new Scripting tab, click ‘Run…’
* Navigate to SCION and make sure ADD TOOLBAR BUTTON is checked. Click Run.
* Click OK on the dialog box that pops up (this will pop up  every time you run the script)
* Click butotn on toolbar to run the script any time. The console box must be open and both layers must be in JOSM for this to work.

##### Usage 
###### MISSING NAMES
* Names that are in INEGI that are not in OSM (Missing)
* These need to be added
* Ideally, there should be no names in this list after following the workflow unless the road/way was intentionally not drawn in, the OSM name is the most complete name, etc. 
###### OSM/LOCAL NAMES
* Names in OSM that do not have a 1:1 match in INEGI (after filters)
* These can be names locals put in, names that are from another task, or have spelling differences, among others. 
* This list can have names in it, just be aware of which ones
###### ALL NAMES IN OSM 
* All names that are currently in OSM. Useful tool for finding edge cases & to see how you are leaving name data
* This list includes names and alt_names 
###### Applying missing names
* Highlight a name
* CMD + F in JOSM on correlating layer, paste name in search bar and hit 'search'
* '3' to zoom to way with that name
* Copy/paste name from INEGI into OSM    

##### Current Features & Filters
This script does not alter data directly in JOSM or OSM. It is comparing 2 lists and outputting the results to a console. A name could either be name= or alt_name=.  
1. Case insensitive comparison by default. There is a case sensitive compare that is commented out if needed. (Finds differences in spacing, spelling, and accents)
2. Displays the number of missing names and the number of OSM names that do not have a match (after filters). Displays total number of OSM names (with no filters)
3. If a name starts with double prefixes and the first is 'Calle': drops 'Calle'
4. If a name starts with identical double prefixes: drops the first prefix (e.g. Privada Privada Águilas is displayed as Privada Águilas)
5. If a name is ONLY 2 prefixes, regardless of if they are identical: ignore rules 3 and 4 and display the name (e.g. Calle Privada, Privada Privada)
6. Will not indicate that names that have no meaning are missing. Currently: Sin Nombre, Falta Nombra, Ninguno, No Aplica, No Disponible
7. If INEGI has a name that is inside a matching pair of parentheses ( + ): display both names (e.g. 'Calle 5ta (Quinta)' will be displayed as 'Calle 5ta' and 'Quinta' on separate line)
8. If INEGI has a name that includes any number of 'Y/O': split the name at each occurance and treat each split as a separate name, also removes 'Y/O' from the names
9. If INEGI has a name that includes any number of commas: split the name at each occurance and treat each split as a separate name, also removes commas from the names
10. If OSM has a name that includes any number of semicolons: split the name at each occurance and treat each split as a separate name, also removes the semicolons from names
11. Prints a list of all names in OSM. Commonly used to review what name related data is being left. 

* Note: Do NOT override local data with INEGI data. Be respectful and preserve as much local data as possible. 