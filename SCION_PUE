// Script that Compares INEGI and OSM Names
//gets the data layer
function data_layer(){
    var layers1 = require("josm/layers");
    var dataLayer = layers1.get("OSM Data"); 
    return dataLayer;
}

//gets the name layer
function name_layer(){
    var layers2 = require("josm/layers");
    var nameLayer = layers2.get("puebla"); 
    return nameLayer;
}

//filters out empty strings
function filter_array(thisArray) {
    var index = -1,
        arr_length = thisArray ? thisArray.length : 0,
        resIndex = -1,
        result = [];

    while (++index < arr_length) {
        var value = thisArray[index];

        if (value) {
            result[++resIndex] = value;
            }
        }
    return result;
}

//removes duplicates in the same array
function remove_duplicates(thisArray) {
   var x = [];
    thisArray.forEach(function(i) {
        if(!x[i]) {
            x[i] = true;
        }
    }
    )
  return Object.keys(x);
};

//The Big Function
function get_names(){
    var dataLayer= data_layer();
    var nameLayer= name_layer();

    //Required
    var util = require("josm/util");
    var command = require("josm/command");
    var console = require("josm/scriptingconsole");

    //Get data from data layer
    var dataDataset= dataLayer.data;
    var dataResult = dataDataset.query("type:way");
    var firstLayer = [];

    //Get data from name layer
    var dataNamelayer = nameLayer.data;
    var nameResult = dataNamelayer.query("type:way");
    var secondLayer = [];
    var footpath = []; //footways and paths
    var steps = [];

    //Store names/alt_names for each layer in a new array 
    for (j = 0; j < dataResult.length; j++) {
        var way = dataResult[j];
        if (way.tags.highway && way.tags.alt_name){
            var highway = (way.tags.highway && way.tags.alt_name);
            firstLayer.push(highway);
        }
        if (way.tags.highway && way.tags.name){
            var highway = (way.tags.highway && way.tags.name);
            var highwayName = (way.tags.highway + " " + way.tags.name)
            firstLayer.push(highway);
        }
    }

    for (i = 0; i < nameResult.length; i++) {
        var way = nameResult[i];
        if (way.tags.highway && way.tags.name){
            var highway2 = (way.tags.highway && way.tags.name);
            secondLayer.push(highway2);
        }
    }

    //Sorting the arrays alphabetically 
    firstLayer.sort();
    secondLayer.sort();

    //Removing duplicates before comparing
    var outputFirst = [];
    var outputSecond = [];
    var count = 0;
    var start = false;

    for (j = 0; j< firstLayer.length; j++){
        for (k = 0; k < outputFirst.length; k++){
            if (firstLayer[j] == outputFirst[k]){
                start = true;
            }
        }
        count++;
        if (count ==1 && start ==false){
            outputFirst.push(firstLayer[j]);
        }
        start = false;
        count = 0
    }

    for (j = 0; j< secondLayer.length; j++){
        for (k = 0; k < outputSecond.length; k++){
            if (secondLayer[j] == outputSecond[k]){
                start = true;
            }
        }
        count++;
        if (count == 1 && start == false){
            outputSecond.push(secondLayer[j]);
        }
        start = false;
        count = 0
    }

    //if OSM name has semi colon, split at each and push to list to be compared
    for (i = 0; i < outputFirst.length; i++){
        var y = outputFirst[i];

        if (y.includes(";")){
            var semiSplit = y.split(";");
            outputFirst[i] = '';
            for (k = 0; k < semiSplit.length ; k++){
                outputFirst.push(semiSplit[k])
            }
        }
    }

    //Splits name at first space = first prefix, slice name for name with one less prefix, splits one less at first space = second prefix
    //Compare prefixes, if match, replace name in list with name with one less prefix  
    for (i = 0; i < outputSecond.length; i++){

        var x = outputSecond[i];

        //If there are parentheses: 1. push name within matching parentheses to outputSecond (missing alt_name)
        //2. remove text and parentheses from name and push
        var regExp = x.match(/\((.*?)\)/);
        if (regExp){
            var submatch = regExp[1];
            outputSecond.push(submatch);
            outputSecond[i] = outputSecond[i].replace(/\s*\(.*?\)\s*/g, '');
        }
        //If name includes 'Y/O', split into separate names  
        if (x.includes("Y/O")){
            var yoSplit = x.split("Y/O ");
            for (k = 1; k < yoSplit.length; k++){
                outputSecond.push(yoSplit[k]);
                outputSecond[i] = outputSecond[i].replace("Y/O " + yoSplit[1], '');
            }
        }
        //If name includes a comma, split into separate names  
        if (x.includes(", ")){
            var commaSplit = x.split(", ");
            x = "";
            for (k = 0; k < commaSplit.length ; k++){
                outputSecond.push(commaSplit[k]);
            }
        }

        var prefix1 = x.split(' ')[0];
        var oneLessPrefix = x.slice(prefix1.length +1);
        var prefix2 = oneLessPrefix.split(' ')[0];
        var spaceCount = (x.split(' ').length - 1);
        var calle = ["Calle"];
        var prefixes = ["Ampliación", "Andador", "Autopista", "Avenida", "Boulevard", "Callejón", "Calzada", "Camino", "Carretera", "Cerrada", "Circuito", "Circulo", "Entrada", "Entronque", "Escalinata", "Peotonal", "Privada", "Prolongación", "Retorno"];
        var nothings =["Sin Nombre", "Falta Nombra", "Ninguno", "No Aplica", "No Disponible"];

        //If a name is only 2 prefixes check: if there is only one space do nothing, else continue
        if(spaceCount == 1){
            outputSecond[i] = outputSecond[i];
        }

        else{
            //Checks for names that have no meaning 
            if(spaceCount >= 0){
                for (k = 0; k < nothings.length; k++){
                    if(x.includes(nothings[k])){
                        outputSecond[i] = "";
                    }
                }
            }

            //If first prefix is Calle and there is a second prefix, drop the calle
            if (prefix1.equals(calle) == true ){
                for (k = 0; k < prefixes.length; k++){
                    if(spaceCount > 1){
                        if (prefixes[k] === prefix2){
                            outputSecond[i] = oneLessPrefix; 
                        }
                    }
                    else {
                        outputSecond[i] = outputSecond[i];
                    }
                } 
            }

            //If the first 2 words are idenitical drop the first one
            if (prefix1.equals(prefix2) == true){
            outputSecond[i] = oneLessPrefix;
            }

            else{
            }
        }
    }

    //Final Stretch
    //Filter out empty strings before acting on arrays
    var outputFirst = filter_array(outputFirst);
    var outputSecond = filter_array(outputSecond);

    var outputFirst = remove_duplicates(outputFirst);

    outputFirst.sort();
    outputSecond.sort();

    var noDupData = [];
    var noDupName = [];

    var lowerFirst = [];
    var lowerSecond = [];

    //Case insensitive
    for (i = 0; i < outputFirst.length; i++){
        lowerFirst[i] = outputFirst[i].toLowerCase();
    }
    for (i = 0; i < outputSecond.length; i++){
        lowerSecond[i] = outputSecond[i].toLowerCase();
    }

    //Compare case insensitive names and push case sensitive name to noDup
    for (var i in outputFirst){
        if(lowerSecond.indexOf(lowerFirst[i]) === -1) noDupData.push(outputFirst[i]);
    } 
    for (var i in outputSecond){
        if(lowerFirst.indexOf(lowerSecond[i]) === -1) noDupName.push(outputSecond[i]);
    }

    //Case Sensitive Compare:Uncomment this and comment out Case Insensitive and Case Insensitive Compare to use 
    /*for (var i in outputFirst){
        if(outputSecond.indexOf(outputFirst[i]) === -1) noDupData.push(outputFirst[i]);
    }
    for (var i in outputSecond){
        if(outputFirst.indexOf(outputSecond[i]) === -1) noDupName.push(outputSecond[i]);
    }
    */

    noDupData.sort;
    noDupName.sort;

    //THE CONSOLE OUTPUT
    console.clear();
    console.println("                              SCION 2.0");
    console.println("----------------------------------------");
    console.println("------------MISSING NAMES------------ " + "(" + noDupName.length + ")");
    console.println("----------------------------------------");
    for (i = 0; i < noDupName.length; i++){
        console.println(noDupName[i]);
    }
    console.println("----------------------------------------");
    console.println("-----------OSM / LOCAL NAMES--------- " + "(" + noDupData.length + ")");
    console.println("----------------------------------------");
    for (i = 0; i < noDupData.length; i++){
        console.println(noDupData[i]); 
    }
    console.println('                                      ...');
    console.println("*****************************************************");
    console.println("***************ALL NAMES IN OSM************** " + "(" + outputFirst.length + ")");
    console.println("*****************************************************");
    for (i = 0; i < outputFirst.length; i++){
        console.println(outputFirst[i]); 
    }
    
}
get_names();